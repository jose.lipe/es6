// Classe
class Client {
  constructor (name, email) {
    this.name = name;
    this.email = email;
  }
}

// Arraw function 1
var color = () => console.log("red");
color();

// Arraw function 2
var bob = {
  _name: "Bob",
  _friends: ["Guilherme", "Camila", "Murilo"],
  printFriends: function printFriends () {
    this._friends.forEach(f => console.log(this._name + " knows " + f));
  }
};

bob.printFriends();


// Uso de Classe
class Color {
  constructor (codeColor, nameColor) {
    this.codeColor = codeColor;
    this.nameColor = nameColor;
  };

  getColor () {
    return {
      codeColor: this.codeColor,
      nameColor: this.nameColor
    };
  }
}

let red = new Color("#ER44HH", "Red");
console.log(red.getColor());


// Template String
let name = "José Felipe";
let age = 28;

let text =`
  name: ${name}
  age: ${age}
`;

console.log(text);


// Valores Default
function test (valueA, valueB = 4) {
  return valueA + valueB;
}

console.log(test(2));



// For OF
var list = [
  "teste",
  34,
  { name: "José" }
];

for (let i of list) {
  console.log(i);
}



// Herança
class People {
  constructor (name, email, phone) {
    this.name = name;
    this.email = email;
    this.phone = phone;
  }
  toString () {
    return `
      Name: ${this.name}
      Email: ${this.email}
      Phone: ${this.phone}`;
  }
}

class Client extends People {
  constructor (id, name, email, phone) {
    super(name, email, phone);
    this.id = id;
  };
  toString () {
    return `
      id: ${ this.id } ${super.toString()}`;
  }
}

var test = new Client(1, "José Felipe", "test@test.com", "51998999076");
console.log(test.toString());



// Set Data Structure
let color = new Set();
color.add("red").add("green").add("red");

if (color.size === 2 && color.has("red")) {
  for (let key of color.values()) {
    console.log(key);
  }
}



// Formatação de Números
var number = 123456.34;
var En = new Intl.NumberFormat('en-US').format(number);
var De = new Intl.NumberFormat('de-DE').format(number);

alert(En);
alert(De);



// Formatação de Moedas
var USD = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(19888999.40);
var BRL = new Intl.NumberFormat("pt-BR", {style: "currency", currency: "BRL"}).format(19888999.40);

alert(USD);
alert(BRL);



// Formatação de datas
var US = new Intl.DateTimeFormat("en-US");
var BR = new Intl.DateTimeFormat("pt-BR");
var DE = new Intl.DateTimeFormat("de-DE");

alert(US.format(new Date("2018-07-18")));
alert(BR.format(new Date("2018-07-18")));
alert(DE.format(new Date("2018-07-18")));
